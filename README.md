# FinalTask
Hello, this is a report about my final task in the course " Telecommunication Software"

**Description**
I decided to focus on the idea of login. But I also add a sign in button to permit users to add users to the database. As I already work with API in this course, this was a good goal to code it. To code it I chose to use React.js ( and a bit of CSS) . The connections with the database will work with Axios. So I will add users to my database and I will also check when the user password and username is linked. If people are connected to the server. I will let them access a dashboard. To complexify it I will add Json Web Tokens which permits to add security.  I think It is a good way to secure the data but I am not expert on it 

**Instruction**

This project is coded with react.js

In this project there is two main parts :
-A server 
-A public interface

As it was asked, I don't publish the repertory directly ( to not add node_modules) 

The project is mainly a login interface. It permit to public to log to the server and to sign up  on it.

To start the server you need to create a directory with the nodes modules and add the code on it.
After that go on the directory
and launch ther server with -npm start


To start the public interface create a directory with :
the nodes modules
the public directory
the package etc...
the src directory

and start it with -npm start

The code on the main is the src directory but I don't know how to change it


**Tests results**
How many new users create an account ?
How many people login ?

To answer those questions I will check the number of requests, and the number of errors received.


